const {dialog, getCurrentWindow} = require('electron').remote
const {ipcRenderer} = require('electron')

function newSheetButtonClick()
{   dialog.showOpenDialog({properties: ['openFile']}).then(result => 
    {   if(result.canceled) {console.log("Open Dialog Canceled"); return}
        console.log("Opening file: " + result.filePaths[0])
        ipcRenderer.send("new-sheet", result.filePaths[0])
    })
    console.log("Button Click")
}
document.querySelector('#newSheet').addEventListener('click', () => {newSheetButtonClick()})
ipcRenderer.on("windowID", (event, ID) => {console.log("Browser-reported windowID: " + ID)})
console.log("Window ID: " + getCurrentWindow().id)