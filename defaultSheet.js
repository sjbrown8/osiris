const {dialog, getCurrentWindow} = require('electron').remote
const {ipcRenderer} = require('electron')

console.log("Window ID: " + getCurrentWindow().id)
ipcRenderer.on("initialDataLoad", (event, initialData) =>
{   document.querySelector('#output').innerHTML = initialData
    console.log(initialData)
})