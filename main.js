// Modules to control application life and create native browser window
const {app, BrowserWindow, ipcMain, dialog} = require('electron')
const path = require('path')
const fs = require('fs')

let windowsArray = []

function createWindow (templateFile, initialData) 
{  console.log("Creating window....")
   newWindow = new BrowserWindow({width: 800,
                                  height: 600,
                                  webPreferences: {preload: path.join(__dirname, 'preload.js'),
                                                   nodeIntegration: true}
                                 })
    newWindow.loadFile(templateFile)
    newWindow.webContents.openDevTools()
    newWindow.webContents.on('did-finish-load', (event, initialData) => 
    {   windowsArray[newWindow.webContents.id] = newWindow
        console.log(initialData)
        newWindow.webContents.send("initialDataLoad", initialData)
    })
    newWindow.on('closed', function () {newWindow.object = null})
}

ipcMain.on("new-sheet", (event, gameDefinitionFile) => 
{    console.log("Loading " + gameDefinitionFile)
     let gameDefContents
     fs.readFile(gameDefinitionFile, 'ascii', (err, gameDefContents) => {})
     createWindow("defaultSheet.html", gameDefContents)
})


app.on('ready',             (event) =>  {createWindow("rootWindow.html")})
app.on('window-all-closed', function () {if (process.platform !== 'darwin') app.quit()})
app.on('activate',          function () {if (windowsArray[0].object === null) createWindow()})
